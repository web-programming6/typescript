const numbers = [1, 2, 3]; //inferred to type number[]
numbers.push(4);
// numbers.push("4");

numbers.forEach(function (values, index) {
  console.log(values + " " + index);
});
