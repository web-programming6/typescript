function getTime(): number {
  return new Date().getTime();
}
console.log(getTime());

function printHello(): void {
  console.log("Hello");
}
printHello();

function multiply(a: number, b: number): number {
  return a * b;
}
console.log(multiply(4, 5));

function add(a: number, b: number, c?: number): number {
  return a + b + (c || 0);
}
console.log(add(1, 2));
console.log(add(1, 2, 3));

function pow(value: number, exponent: number = 10): number {
  return value ** exponent;
}
console.log(pow(2));
console.log(pow(2, 2));

function devide({ divided, divisor }: { divided: number; divisor: number }) {
  return divided / divisor;
}
console.log(devide({ divided: 10, divisor: 100 }));

function addRest(a: number, b: number, ...rest: number[]) {
  return a + b + rest.reduce((p, c) => p + c, 0);
}
console.log(addRest(10, 10, 10, 10, 10, 10));

type Negate = (value: number) => number;
const negateFunction: Negate = (value) => value * -1;
const negateFunction2: Negate = function (value: number): number {
  return value * -1;
};
console.log(negateFunction(10));
console.log(negateFunction2(10));
