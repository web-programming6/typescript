class Person2 {
  public constructor(private readonly name1: string) {}

  public getName(): string {
    return this.name1;
  }
}

const person2 = new Person2("Jane");
console.log(person2);
console.log(person2.getName());
