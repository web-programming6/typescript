const car: { type: string; model: string; year: number } = {
  type: "Toyota",
  model: "Corolla",
  year: 2009,
};
console.log(car);

const people: { name: string; surname: string; middle?: string } = { //middle? is optional properties to not assign middle
  name: "Kittipon",
  surname: "Thaweelarb",
};
console.log(people);
