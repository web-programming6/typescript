// Explicit Type
let explicitFirstname: String = "Kittipon";
console.log(explicitFirstname);
console.log(typeof explicitFirstname);
// Implicit Type
let implicitSurname = "Thaweelarb";
console.log(implicitSurname);
console.log(typeof implicitSurname);
