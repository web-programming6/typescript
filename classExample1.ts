class Person1 {
  name: string;

  public constructor(name: string) {
    this.name = name;
  }

  public getName(): string {
    return this.name;
  }
}

const person1 = new Person1("Jane");
console.log(person1);
console.log(person1.getName());
