const names: string[] = [];
// const readonly: readonly string[] = []; **add readonly to only read array**
// const numbers = [1, 2, 3]; **inferred to type number[]**

names.push("Kittipon");
names.push("Natthakritta");
names.push("Thanataya");
// names.push(3); **error with incorrect types**

console.log(names[0]);
console.log(names[1]);
console.log(names.length);

console.log("\nprint name @ i");
for (let i = 0; i < names.length; i++) {
  console.log(names[i]);
}

console.log("\nprint name for each");
for (let index in names) {
  console.log(names[index]);
}

console.log("\nprint name");
names.forEach(function (name) {
  console.log(name);
});
