//unknown
let w: unknown = 1;
w = "String";
//object
w = {
  runANonExistenMedthod: () => {
    console.log("I think therefore I am");
  },
} as { runANonExistenMedthod: () => void };

if (typeof w === "object" && w != null) {
  (w as { runANonExistenMedthod: Function }).runANonExistenMedthod();
}
