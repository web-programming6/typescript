let x1: unknown = "hello";
console.log(x1);
console.log("length : " + (x1 as string).length);
console.log("length : " + (<string>x1).length);

let num: string = "1";
console.log((num as unknown as number) + 5);
