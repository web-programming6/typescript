interface Rectangle {
  height: number;
  width: number;
}
interface ColorRectangle extends Rectangle {
  color: string;
}
const rectangle: Rectangle = {
  height: 20,
  width: 10,
};

console.log(rectangle);

const colorRectangle: ColorRectangle = {
  width: 20,
  height: 10,
  color: "green",
};
console.log(colorRectangle);
