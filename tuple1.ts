let ourTuple: [number, boolean, string];
ourTuple = [5, false, "Coding God was here"];

console.log(ourTuple);
console.log(typeof ourTuple);

let readOnlyTuple: readonly [number, boolean, string];
readOnlyTuple = [5, false, "Coding God was here"];
// readOnlyTuple.push("new"); 
console.log(readOnlyTuple);
console.log(typeof readOnlyTuple);
